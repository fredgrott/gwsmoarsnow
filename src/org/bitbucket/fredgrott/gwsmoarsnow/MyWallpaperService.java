package org.bitbucket.fredgrott.gwsmoarsnow;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import org.bitbucket.fredgrott.glwallpaperservicelibrary.GLWallpaperService;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.Log;
import android.view.SurfaceHolder;




// TODO: Auto-generated Javadoc
/**
 * The Class MyWallpaperService.
 */
public class MyWallpaperService extends GLWallpaperService {
	
	/** The Constant windCurve. */
	private final static int[] windCurve = {1,3,5,6,6,6,7,7,6,5,4,2,1,1};
	
	/**
	 * Instantiates a new my wallpaper service.
	 */
	public MyWallpaperService() {
		// TODO Auto-generated constructor stub
	}
	
	/* (non-Javadoc)
	 * @see org.bitbucket.fredgrott.glwallpaperservicelibrary.GLWallpaperService#onCreateEngine()
	 */
	@Override
	public Engine onCreateEngine() {
        SnowEngine engine = new SnowEngine();
        return engine;
    }
	
	/**
	 * The Class SnowEngine.
	 */
	public class SnowEngine extends GLEngine {
		
		/** The renderer. */
		MyWallpaperRenderer renderer;
		
		/** The thread. */
		private SnowThread thread;
        
        /** The random. */
        private Random random = new Random();
        
        /** The holder. */
        private SurfaceHolder holder;

        /** The flake image. */
        private Bitmap flakeImage;
        
        /** The flakes. */
        private Set<Snowflake> flakes = new HashSet<Snowflake>();
        
        /** The oob flakes. */
        private Set<Snowflake> oobFlakes = new HashSet<Snowflake>();
        
        /** The wind. */
        private int wind = 0;

        /**
         * The Class Snowflake.
         */
        class Snowflake {

            /** The bitmap. */
            public Bitmap bitmap;
            
            /** The x. */
            float x;
            
            /** The y. */
            float y;
            
            /** The bias. */
            int bias = 0;
            
            /** The bias time. */
            int biasTime = 0;
            
            /** The inter bias. */
            boolean interBias = false;

            /**
             * Instantiates a new snowflake.
             *
             * @param x the x
             * @param y the y
             * @param bitmap the bitmap
             */
            Snowflake(float x, float y, Bitmap bitmap) {
                this.x = x;
                this.y = y;
                this.bitmap = bitmap;
            }

            /**
             * Update.
             *
             * @param wind the wind
             */
            void update(int wind) {

                if (biasTime < 0) {
                    if (interBias) {
                        bias = ((Integer) random.nextInt(3)).compareTo(1);
                        interBias = false;
                    } else {
                        bias = 0;
                        interBias = true;
                    }

                    biasTime = random.nextInt(30);
                }

                biasTime--;
                x += bias + wind;
                y += 4;
            }
        }
        
        /**
         * The Class SnowThread.
         */
        public class SnowThread extends Thread {

            /** The is snowing. */
            private boolean isSnowing = true;
            
            /** The last wind update. */
            private long lastWindUpdate = System.currentTimeMillis();
            
            /** The wind curve index. */
            private int windCurveIndex = 0;

            /**
             * Update.
             *
             * @param c the c
             */
            private void update(Canvas c) {

                c.drawRGB(0, 0, 0);
                flakes.add(new Snowflake(random.nextFloat() * c.getWidth(), -flakeImage.getWidth(), flakeImage));
                long nowTime = System.currentTimeMillis();

                if (wind == 0) {
                   if (nowTime - lastWindUpdate > 20000) {
                      wind = windCurve[++windCurveIndex];
                      lastWindUpdate = nowTime;
                   }
                } else if (nowTime - lastWindUpdate > 1000) {
                    if (windCurveIndex < windCurve.length -1) {
                        wind = windCurve[++windCurveIndex];
                        lastWindUpdate = nowTime;
                    } else {
                        windCurveIndex = 0;
                        wind = 0;
                        lastWindUpdate = nowTime;
                    }
                }


                for (Snowflake flake : flakes) {
                    flake.update(wind);
                    c.drawBitmap(flake.bitmap, flake.x, flake.y, null);

                    if (flake.x > c.getWidth() || flake.x < -flake.bitmap.getWidth() ||
                            flake.y > c.getHeight()) {
                        oobFlakes.add(flake);
                    }

                }

                flakes.removeAll(oobFlakes);
                oobFlakes.clear();

            }

            /* (non-Javadoc)
             * @see java.lang.Thread#run()
             */
            @Override
            public void run() {
                Log.d("snow", "SnowThread started");
                while (isSnowing) {
                    Canvas canvas = null;
                    try {
                        Thread.sleep(30);
                        canvas = holder.lockCanvas();
                        update(canvas);

                    } catch (InterruptedException e) {

                    } finally {
                        if (canvas != null) {
                            holder.unlockCanvasAndPost(canvas);
                        }
                    }
                }
            }

            /**
             * Checks if is snowing.
             *
             * @return true, if is snowing
             */
            public boolean isSnowing() {
                return isSnowing;
            }

            /**
             * Stop snowing.
             */
            public void stopSnowing() {
                isSnowing = false;
            }
        }

        /* (non-Javadoc)
         * @see org.bitbucket.fredgrott.glwallpaperservicelibrary.GLWallpaperService.GLEngine#onVisibilityChanged(boolean)
         */
        @Override
        public void onVisibilityChanged(boolean visible) {
            super.onVisibilityChanged(visible);
            Log.d("snow", "visibilityChanged to " + visible);
            if (visible) {
                thread = new SnowThread();
                thread.start();
            } else {
                thread.stopSnowing();
            }
        }

        /* (non-Javadoc)
         * @see org.bitbucket.fredgrott.glwallpaperservicelibrary.GLWallpaperService.GLEngine#onSurfaceCreated(android.view.SurfaceHolder)
         */
        @Override
        public void onSurfaceCreated(SurfaceHolder holder) {
            Log.d("snow", "surfaceCreated");
            this.holder = holder;
        }

        /* (non-Javadoc)
         * @see org.bitbucket.fredgrott.glwallpaperservicelibrary.GLWallpaperService.GLEngine#onSurfaceDestroyed(android.view.SurfaceHolder)
         */
        @Override
        public void onSurfaceDestroyed(SurfaceHolder holder) {
            // usual case is onVisibilityChanged(false) has been called
            // and  the thread is shutting down
            Log.d("snow", "surfaceDestroyed");
            boolean retry = true;
            while (retry) {
                try {
                    thread.stopSnowing(); // just in case
                    thread.join();
                    Log.d("snow", "snowThread died");
                    retry = false;
                } catch (InterruptedException e) {
                }
            }
        }
        
        /**
         * Instantiates a new snow engine.
         */
        public SnowEngine() {
        	super();
            flakeImage = BitmapFactory.decodeResource(getResources(), R.drawable.square3x3);
            renderer = new MyWallpaperRenderer();
            setRenderer(renderer);
            setRenderMode(RENDERMODE_CONTINUOUSLY);
        }
        
        /* (non-Javadoc)
         * @see org.bitbucket.fredgrott.glwallpaperservicelibrary.GLWallpaperService.GLEngine#onDestroy()
         */
        public void onDestroy() {
            super.onDestroy();
            if (renderer != null) {
               renderer.release();
           }
           renderer = null;
       }
	}

}
