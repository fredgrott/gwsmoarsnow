package org.bitbucket.fredgrott.gwsmoarsnow;

import org.bitbucket.fredgrott.glwallpaperservicelibrary.*;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

// TODO: Auto-generated Javadoc
/**
 * The Class MyWallpaperRenderer.
 */
public class MyWallpaperRenderer  implements GLWallpaperService.Renderer {
	       
       	/* (non-Javadoc)
       	 * @see android.opengl.GLSurfaceView.Renderer#onDrawFrame(javax.microedition.khronos.opengles.GL10)
       	 */
       	public void onDrawFrame(GL10 gl) {
		        
		          
		           gl.glClearColor(0.2f, 0.4f, 0.2f, 1f);
		           gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
		       }
		       
		       /* (non-Javadoc)
       		 * @see android.opengl.GLSurfaceView.Renderer#onSurfaceChanged(javax.microedition.khronos.opengles.GL10, int, int)
       		 */
       		public void onSurfaceChanged(GL10 gl, int width, int height) {
		       }
		       
		       /* (non-Javadoc)
       		 * @see android.opengl.GLSurfaceView.Renderer#onSurfaceCreated(javax.microedition.khronos.opengles.GL10, javax.microedition.khronos.egl.EGLConfig)
       		 */
       		public void onSurfaceCreated(GL10 gl, EGLConfig config) {
		       }
		       
		       
		       /**
       		 * Release.
       		 */
       		public void release() {
		       }
   }